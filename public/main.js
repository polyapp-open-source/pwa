// This file is a library of functions which can be used in a main thread.
// It should contain ALL interaction with the service worker.

if ('serviceWorker' in navigator) {
    window.addEventListener('load', function() {
        navigator.serviceWorker.register('/sw.js').then(function(registration) {
            // Registration was successful
            console.log('ServiceWorker registration successful with scope: ', registration.scope);
        }, function(err) {
            // registration failed :(
            console.log('ServiceWorker registration failed: ', err);
        });
    });
}

// All outgoing messages match the formats described in serviceWorkerAPIs.go

// Listen to messages from service workers. This handles everything.
navigator.serviceWorker.addEventListener('message', function(event) {
    console.log('Reply from service worker');
    console.log(event);
    if (event.data == null) {
        console.error('message was null');
        return;
    }
    if (event.data.MessageID === "") {
        console.error('no messageID');
        return;
    }
    if (event.data.AddAjaxDOM == null) {
        console.error('AddAjaxDOM is empty - might be OK but currently unhandled');
        return;
    }

    // do the work of modifying the DOM based on the received message
    event.data.AddAjaxDOM.forEach((AjaxDOM) => {
        if (AjaxDOM.DeleteSelector != "") {
            let oldEl = document.querySelector(AjaxDOM.DeleteSelector);
            if (oldEl != null) {
                oldEl.remove();
            }
        }
        if (AjaxDOM.InsertSelector != "" && AjaxDOM != "" && AjaxDOM.HTML != "") {
            let el = document.querySelector(AjaxDOM.InsertSelector);
            if (el == null) {
                return;
            }
            el.insertAdjacentHTML(AjaxDOM.Action, AjaxDOM.HTML);
        }
    });
});

// notifyServiceWorker sends a message to a service worker. It has only been
// tested with textarea and buttons - buttons send a blank message.
function notifyServiceWorker(event) {
    // validate with Go code locally by sending a message to the service worker.
    if (navigator.serviceWorker.controller) {
        // only actually tested with text boxes right now
        let validateTextMessage = {
            MessageID: "lsdlksdk",
            PageContext: "na",
            ID: event.target.id,
            Data: event.target.value,
        };
        console.log('outgoing to service worker:');
        console.log(validateTextMessage);
        navigator.serviceWorker.controller.postMessage(validateTextMessage);
    } else {
        console.log('could not send message to service worker');
    }
}
// example of using validateRequest:
// document.querySelector('#someID').addEventListener('input', validate);

// backend calls the 'secondServer' server - a 2nd web server - directly.
function secondServer(event) {
    let req = new XMLHttpRequest();
    req.open('GET', getSecondServerURL(), true);
    req.onload = function() {
        let responseJSON = JSON.parse(this.responseText);
        let bb = document.getElementById('secondserverButton');
        bb.insertAdjacentHTML('afterend', responseJSON.HTML);

        let b = document.getElementById('second_server_network_error');
        b.hidden = true;
    };
    req.onerror = () => {
        document.getElementsByTagName('body')[0].setAttribute('style', 'margin-top: 50px;');
        let b = document.getElementById('second_server_network_error');
        b.hidden = false
    };
    req.send();
}

// getSecondServerURL helps resolve differences in environments
function getSecondServerURL() {
    if (location.href === 'https://localhost/') {
        return 'https://localhost:7900';
    }
    return "https://backend-dot-polyapp-open-source.appspot.com";
}
