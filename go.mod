module gitlab.com/polyapp-open-source/pwa

go 1.13

require (
	github.com/NYTimes/gziphandler v1.1.1
	github.com/chromedp/cdproto v0.0.0-20200116234248-4da64dd111ac
	github.com/chromedp/chromedp v0.5.3
	github.com/labstack/echo/v4 v4.1.15
	github.com/mailru/easyjson v0.7.0
	github.com/rs/cors v1.7.0
	github.com/spf13/pflag v1.0.5 // indirect
	gitlab.com/polyapp-open-source/poly v0.0.0-20200304172929-90b164ae7520 // indirect
)
