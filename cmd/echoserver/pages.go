package main

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

// pageHome is the home page at index.html
func pageHome(c echo.Context) error {
	return c.Render(http.StatusOK, "index", []int{1, 2, 3})
}
