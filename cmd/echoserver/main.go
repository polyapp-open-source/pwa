// Command echoserver runs a simple web server using Echo.
// More information about Echo: https://github.com/labstack/echo
// Echo is used because it's so easy to add its middleware, some of which is necessary
// for a perfect Lighthouse audit score. Its use of templates also helps a lot with modularizing code.
package main

import (
	"fmt"
	"html/template"
	"io"
	"net/http"
	"os"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/polyapp-open-source/pwa/common"
)

// webserver can serve static files to clients over HTTP or HTTPS on localhost
// Building uses https://gitlab.com/polyapp-open-source/poly. Just run 'poly build'. Run with 'poly run'.
func main() {
	e := createMux()
	port, portExists := os.LookupEnv("PORT")
	// TLSPATH is only relevant in local development
	TLSPath, TLSPathExists := os.LookupEnv("TLSPATH")
	if !TLSPathExists {
		if !portExists {
			// if you do not use port 80 and instead use, say, 8080
			// you may get an interstitial error from Chrome when running your audits
			// if you change this, you'll also likely want to change the TLS port
			// if you change that, you'll likely want to add e.Pre(middleware.HTTPSRedirect()) in createMux()
			port = "80"
		}
		e.Logger.Fatal(e.Start("localhost:" + port))
	} else {
		if !portExists {
			port = "443"
		}
		// redirect HTTP -> HTTPS on port 80. You need this for a perfect score
		go func() {
			panic(http.ListenAndServe(":80", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				http.Redirect(w, r, "https://"+r.Host+r.URL.String(), http.StatusMovedPermanently)
			})))
		}()
		cert := TLSPath + string(os.PathSeparator) + "server.crt"
		key := TLSPath + string(os.PathSeparator) + "server.key"
		e.Logger.Fatal(e.StartTLS("localhost:"+port, cert, key))
	}
}

// createMux
func createMux() *echo.Echo {
	e := echo.New()
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(middleware.Gzip())
	e.Use(middleware.Secure())
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{common.AppEngineSecondServerURL, common.LocalhostSecondServerURL},
		AllowMethods: []string{http.MethodGet, http.MethodPut, http.MethodPost, http.MethodDelete},
	}))
	t := &Template{
		templates: template.Must(template.ParseGlob("public/views/*.html")),
	}
	e.Renderer = t
	// a lot of content can be served directly out of the 'public' folder
	// this would include things like icons and manifest.json
	// you might also consider caching these paths in CDNs, although that will make updates harder
	e.Static("/", "public")
	// this handler is called by Google's App Engine to 'warm up' your app before traffic starts to arrive.
	e.GET("/_ah/warmup", func(c echo.Context) error {
		return nil
	})
	e.GET("/", pageHome)
	e.GET("/index.html", pageHome)
	e.POST("/messageEvent", messageHandler)
	return e
}

// messageHandler is the server-side equivalent of the client's SimpleMessageEventHandler. It handles message events
// from the DOM and replies to the client with a JSON-encoded MessageOutgoing.
func messageHandler(c echo.Context) error {
	messageIncoming := new(common.MessageIncoming)
	err := c.Bind(messageIncoming)
	if err != nil {
		return err
	}
	messageOutgoing, err := common.HandleMessageIncoming(*messageIncoming)
	if err != nil {
		return fmt.Errorf("HandleMessageIncoming failed: %w", err)
	}

	return c.JSON(http.StatusOK, messageOutgoing)
}

type Template struct {
	templates *template.Template
}

func (t *Template) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	return t.templates.ExecuteTemplate(w, name, data)
}
