// +build js

package main

import (
	"errors"
	"fmt"
	"sync"
	"syscall/js"

	"gitlab.com/polyapp-open-source/pwa/common"
)

// GetMessage translates an incoming message to the MessageIncoming format or throws an error.
func GetMessage(this js.Value, args []js.Value) (messageIncoming common.MessageIncoming, err error) {
	messageData := args[0].Get("data")
	if !messageData.Truthy() {
		return messageIncoming, errors.New("messageData wasn't truthy")
	}
	if !messageData.Get("MessageID").Truthy() {
		return messageIncoming, errors.New("MessageID not included in message")
	}
	if !messageData.Get("PageContext").Truthy() {
		return messageIncoming, errors.New("PageContext not included in message")
	}
	if !messageData.Get("ID").Truthy() {
		return messageIncoming, errors.New("ID not included in message")
	}
	if !messageData.Get("Data").Truthy() {
		messageData.Set("Data", "") // hacky
	}
	messageIncoming = common.MessageIncoming{
		MessageID:   messageData.Get("MessageID").String(),
		PageContext: messageData.Get("PageContext").String(),
		ID:          messageData.Get("ID").String(),
		Data:        messageData.Get("Data").String(),
	}
	err = messageIncoming.Validate()
	if err != nil {
		return common.MessageIncoming{}, fmt.Errorf("invalid incoming message: %w", err)
	}
	return messageIncoming, nil
}

func SetMessage(messageOutgoing common.MessageOutgoing) js.Value {
	messageObject := js.Global().Get("Object").New()
	messageObject.Set("MessageID", messageOutgoing.MessageID)

	// I'm doing this work to get an array of objects so that js.ValueOf works properly
	addAjaxDOMLocal := make([]interface{}, len(messageOutgoing.AddAjaxDOM))
	for i, a := range messageOutgoing.AddAjaxDOM {
		ajaxDOMObject := js.Global().Get("Object").New()
		ajaxDOMObject.Set("InsertSelector", a.InsertSelector)
		ajaxDOMObject.Set("DeleteSelector", a.DeleteSelector)
		ajaxDOMObject.Set("PageContext", a.PageContext)
		ajaxDOMObject.Set("Action", a.Action)
		ajaxDOMObject.Set("HTML", a.HTML)
		addAjaxDOMLocal[i] = ajaxDOMObject
	}
	messageObject.Set("AddAjaxDOM", js.ValueOf(addAjaxDOMLocal))
	return messageObject
}

var (
	globalMessage js.Value
	messageLock   sync.Mutex
)

// SendMessage sends a message to all listening clients.
func SendMessage(messageOutgoing common.MessageOutgoing) error {
	err := messageOutgoing.Validate()
	if err != nil {
		return fmt.Errorf("MessageOutgoing is invalid: %w", err)
	}
	self := js.Global().Get("self")
	if !self.Truthy() {
		return errors.New("couldn't get self")
	}

	messageLock.Lock()
	globalMessage = SetMessage(messageOutgoing)
	self.Get("clients").Call("matchAll").Call("then", js.FuncOf(sendMessageToClientList))
	messageLock.Unlock()
	return nil
}

func sendMessageToClientList(this js.Value, args []js.Value) interface{} {
	clientList := args[0]
	clientList.Call("forEach", js.FuncOf(sendMessageToClient))
	return nil
}

func sendMessageToClient(this js.Value, args []js.Value) interface{} {
	client := args[0]
	client.Call("postMessage", globalMessage)
	return nil
}

// ConsoleLog logs a single js.Value to console. This is prettier than using fmt with js values.
func ConsoleLog(v js.Value) {
	js.Global().Get("console").Call("log", v)
}
