// +build js

// Command serviceworker is Go code compiled to WASM which can respond to requests
// you might normally make to a web server. This might include validation requests,
// performing inference or other ML in the browser, or any other unprivileged work.
// It does NOT have access to the DOM, but it DOES have the ability to send messages
// in to the main thread.
package main

import (
	"fmt"
	"syscall/js"

	"gitlab.com/polyapp-open-source/pwa/common"
)

var (
	endlessChannel chan string
)

func main() {
	js.Global().Get("self").Set("messageEventHandler", js.FuncOf(simpleMessageEventHandler))
	// run this code indefinitely
	endlessChannel = make(chan string, 0)
	<-endlessChannel
}

// simpleEventListener is an example of a message event handler
func simpleMessageEventHandler(this js.Value, args []js.Value) interface{} {
	messageIncoming, err := GetMessage(this, args)
	if err != nil {
		fmt.Println("GetMessage error: " + err.Error())
	}
	messageOutgoing, err := common.HandleMessageIncoming(messageIncoming)
	if err != nil {
		fmt.Println("Error in HandleMessageIncoming: " + err.Error())
		return nil
	}
	err = SendMessage(messageOutgoing)
	if err != nil {
		fmt.Println("Error in SendMessage: " + err.Error())
		return nil
	}
	return nil
}
