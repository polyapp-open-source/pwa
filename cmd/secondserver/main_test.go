// +build !js

package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestSimpleReply(t *testing.T) {
	req := httptest.NewRequest("GET", "http://localhost:7900", nil)
	w := httptest.NewRecorder()
	SimpleReply(w, req)

	resp := w.Result()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatalf("Error reading response body: %s", err.Error())
	}
	resp.Body.Close()

	var cR componentReply
	err = json.Unmarshal(body, &cR)
	if err != nil {
		t.Errorf("Error in Unmarshal: %v", err.Error())
	}

	if cR.HTML != `<p id="secondserverhello">Hello from the Second Server!</p>` {
		t.Errorf("SimpleReply should always place the Hello string into the response body: %v", cR)
	}
	if resp.StatusCode != http.StatusOK {
		t.Error("Should have gotten back a 200 - OK")
	}
}
