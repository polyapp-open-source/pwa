// +build !js

// Command secondserver runs a web server which acts like something of a data backend.
package main

import (
	"encoding/json"
	"fmt"
	"net"
	"net/http"
	"os"
	"runtime"
	"time"

	"github.com/rs/cors"
)

func main() {
	mux := http.NewServeMux()
	mux.HandleFunc("/", SimpleReply)
	handler := cors.New(cors.Options{
		AllowCredentials: true,
		AllowedMethods:   []string{"GET", "POST", "PUT"},
		AllowedOrigins:   []string{"*"}, // obviously insecure
		AllowedHeaders:   []string{"Accept", "Accept-Language", "Accept-Encoding", "Content-Type", "Content-Length", "Authorization"},
		MaxAge:           120,
	}).Handler(mux)

	port, portExists := os.LookupEnv("PORT")
	if !portExists {
		// I'm setting port to 7900 to make this consistent to help with my testing
		port = "7900"
	}

	server := &http.Server{
		Addr:           ":" + port,
		Handler:        handler,
		IdleTimeout:    60 * time.Second,
		ReadTimeout:    60 * time.Second,
		WriteTimeout:   60 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	// check if something is already on the port we're using and if so, kill it.
	listener, err := net.Listen("tcp", "localhost"+server.Addr)
	if err != nil || listener == nil {
		if runtime.GOOS == "windows" {
			fmt.Println("Error opening port " + server.Addr + ". On Windows, open Task Manager and kill webserver")
		} else {
			fmt.Println("Error opening port " + server.Addr + ". Try running `killall webserver` ")
		}
		fmt.Printf("Second Server failed to start. Error: %v ; listener: %v \n", err, listener)
		return
	}
	err = listener.Close()
	if err != nil {
		panic("Couldn't close the test listener: " + err.Error())
	}

	// I use this to serve HTTPS on my Windows machine and WSL. If you don't set TLSPATH it should serve over HTTP.
	// TODO pass credentials to the container so the E2E tests pass
	tlsPath := ""
	if runtime.GOOS == "windows" {
		tlsPath = os.Getenv("TLSPATH") + "\\\\"
	} else {
		tlsPath = os.Getenv("TLSPATH") + string(os.PathSeparator)
	}
	fmt.Printf("Second Server is serving on port: %v ; TLSPATH (HTTPS certificate directory): %v \n", server.Addr, tlsPath)
	if tlsPath == string(os.PathSeparator) || tlsPath == "\\\\" {
		panic(server.ListenAndServe())
	} else {
		panic(server.ListenAndServeTLS(tlsPath+"server.crt", tlsPath+"server.key"))
	}
}

// componentReply is a simple structure which contains what we're replying to the client with
// it is the same as in frontend/main.go
// TODO: use the same structure in client and server. Not implmeneted right now for simplicty.
type componentReply struct {
	HTML string
}

// SimpleReply always replies with componentReply marshaled into JSON and with HTML set to "<p>Hello from the Backend!</p>".
func SimpleReply(w http.ResponseWriter, r *http.Request) {
	replyData := componentReply{
		HTML: `<p id="secondserverhello">Hello from the Second Server!</p>`,
	}
	marshalledReply, err := json.Marshal(replyData)
	if err != nil {
		panic(err)
	}
	_, err = w.Write(marshalledReply)
	if err != nil {
		panic(err)
	}
}
