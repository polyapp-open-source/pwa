[![pipeline status](https://gitlab.com/polyapp-open-source/pwa/badges/master/pipeline.svg)](https://gitlab.com/polyapp-open-source/pwa/-/commits/master)
[![go report](https://goreportcard.com/badge/gitlab.com/polyapp-open-source/pwa)](https://goreportcard.com/report/gitlab.com/polyapp-open-source/pwa)
[![license](https://img.shields.io/badge/license-MIT-green)](https://gitlab.com/polyapp-open-source/pwa/-/blob/master/LICENSE)

# Purpose
This page is deployed at https://pwa.polyapp.tech
* Achieve a perfect lighthouse score.
* Re-use server-side code in a service worker to allow your page to respond faster and work offline.

# Things I have learned
* Go code should NOT need to load for the main thread to work because of the large bundle size which creates
a horrible 'time til interactive' on slow connections.
* Using TinyGo is a possibility, but for a truly fast website I still wouldn't recommend it being 'required'
to run before being able to interact with the website. 100kB bundles are still big for some mobile networks.
* If you really want to use TinyGo, just remember you're also introducing a potential code problem - as
more features are added Go's bundle size increases faster than an equivalent JS bundle size, so it doesn't scale as well.
* If you STILL want to use TinyGo, just know the syntax is different than regular Go so you'll have to rewrite your standard
Go WASM code to support this other syntax. So you can't switch back and forth. And the syntax updates occasionally too.
* Regular Go code's bundle size - >1 MB - is so large no mobile website should ever be forced to load it for the page to work.
* By duplicating logic in WASM and at a particular server URL, you can overcome this problem.
I have done this. WASM code is in cmd/serviceworker/. Common code is in common/. Server code is in cmd/echoserver/.
* By communicating with WASM solely through service worker messages and directing 'messages' to the server
if the WASM code isn't ready, you can achieve a common code path through which to handle this duplicate logic.
I have done this. public/views/index.html adds event listeners to buttons and a text box.
public/main.js includes the functions called during events which post messages to the service worker.
public/sw.js receives those messages and tries to call a WASM function attached to 'self' in Go code which can reply to the message.
It also has a fallback: calling the server directly (as previously mentioned, WASM functionality is duplicated on the server).
* I would NOT recommend EVER writing Go code which is just JS code. You will get all of the problems of JS (like no type safety
from syscall/js), and you get fun new problems like extremely verbose code, no editor support, and not being able to use JS shorthand.
* I have similar opinions about generating HTML in Go - you're learning an extra syntax, no longer
allowing your editor's tooling for HTML to help you, and generally making life harder for yourself. Instead, use Go's templates.
* I WOULD recommend using Go code when you had planned to write JS which mimicked Go code. My examples for this are
doing validation on a value as you type (the textarea on pwa.polyapp.tech) and inserting templated HTML code (as in some AJAX patterns).
I do this when you click the 'add tab' button, and removing the code when clicking 'remove tab'.
