#!/usr/bin/env bash

# You're probably wondering why this exists - people know how to generate certificates, why do we need this script?
# I want to serve 'secondserver' with HTTPS in PROD and I want to run my tests like I run PROD without configuration files
# However a Stack Overflow security post recommended against passing in any credentials to CI/CD so you don't make a habit of it
# It also suggested using a complicated piece of software to handle managing certificates and other such stuff
# But I dislike complicated things. So instead I'm just going to generate a self-signed certificate in the Docker container
# And have the docker container's browser trust it (or, rather, trust all insecure certificates)

# Abort if we already have a certificate path
if [ -z "${TLSPATH}" ] && [ "$TLSPATH" != "" ]; then
  exit 0
fi

# Abort if running in MINGW because this script will produce a path that won't work with go env OS=windows
# Essentially it'll produce something like /c/Users/blah/wherever and Go needs C:\Users\blah\wherever
if [ "$(expr substr $(uname -s) 1 10)" == "MINGW32_NT" ]; then
  exit 0
elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW64_NT" ]; then
  exit 0
fi

# Generate self-signed certificate
openssl req -newkey rsa:2048 -nodes -keyout server.key -x509 -days 365 -out server.crt -sha256 -config openssl.cnf

# Set certificate path in .profile so we can re-read it in the parent
# Parent shells must run some code to get the update: source ~/.profile
TLSPATH=$PWD
echo "export TLSPATH=$TLSPATH" >>~/.profile
