package common

const (
	// LocalhostSecondServerURL is used when serving on localhost
	LocalhostSecondServerURL = "https://localhost:7900" // assumes you're using HTTPS
	// AppEngineSecondServerURL is used when serving on App Engine
	AppEngineSecondServerURL = "https://backend-dot-polyapp-open-source.appspot.com"
	// AppEngineEchoURL is used when serving on App Engine
	AppEngineEchoURL = "https://pwa.polyapp.tech"
	// AppEngineEchoURLAlt is used when testing with Google's appspot subdomains
	AppEngineEchoURLAlt = "https://polyapp-open-source.appspot.com"
	// LocalhostEchoURL is used when serving on localhost
	LocalhostEchoURL = "https://localhost"
)
