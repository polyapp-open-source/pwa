package common

import (
	"errors"
	"strings"
)

type MessageIncoming struct {
	// MessageID identifies this message. This ensures 1 message gets 1 response.
	MessageID string `json:"MessageID"`
	// PageContext identifies what Page / View this is associated with globally
	PageContext string `json:"PageContext"`
	// ID which identifies what element in the Page / View this is associated with
	ID string `json:"ID"`
	// Data which is Type-specific
	Data string `json:"Data"`
}

func (message MessageIncoming) Validate() error {
	if message.MessageID == "" {
		return errors.New("MessageID is empty")
	}
	if message.PageContext == "" {
		return errors.New("PageContext is empty")
	}
	if message.ID == "" {
		return errors.New("ID is empty")
	}
	if strings.Contains(strings.ToLower(message.ID), "button") {
		// all buttons should be taking this code path
		if message.Data != "" {
			return errors.New("thought we had a button but there was Data passed up")
		}
	} else {
		if message.Data == "" {
			return errors.New("data is empty")
		}
	}
	return nil
}

// ModDOM contains everything the client needs to modify the DOM with 'insertAdjacentHTML' function call.
type ModDOM struct {
	// PageContext is the page where the InsertSelector is found
	PageContext string
	// DeleteSelector is all of the elements you're trying to remove
	DeleteSelector string
	// InsertSelector is the ID of the element we're inserting near
	InsertSelector string
	// Action is the position parameter in 'insertAdjacentHTML'
	Action string
	// HTML is what we are putting in to the DOM. It's the text parameter in 'insertAdjacentHTML'
	HTML string
}

type MessageOutgoing struct {
	// MessageID is a randomly generated string. It's identical to MessageID on the incoming message.
	MessageID string
	// AddAjaxDOM are things we're putting in to the DOM.
	AddAjaxDOM []ModDOM
}

// validate ensures the structure of MessageOutgoing is valid
func (message MessageOutgoing) Validate() error {
	if message.MessageID == "" {
		return errors.New("message didn't have an ID")
	}
	if message.AddAjaxDOM == nil {
		return errors.New("message didn't have initialized AddAjaxDOM")
	}
	for i, a := range message.AddAjaxDOM {
		if a.PageContext == "" {
			return errors.New("no PageContext at index: " + string(i))
		}
		if a.Action != "" && a.Action != "beforebegin" && a.Action != "afterbegin" && a.Action != "beforeend" && a.Action != "afterend" {
			return errors.New("Action name: " + a.Action + " is invalid at index: " + string(i))
		}
	}
	return nil
}
