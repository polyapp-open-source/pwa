package common

import (
	"errors"
	"fmt"
)

func HandleMessageIncoming(incomingMessage MessageIncoming) (MessageOutgoing, error) {
	switch incomingMessage.ID {
	case "validateTextarea":
		messageOutgoing, err := handleValidateTextArea(incomingMessage)
		if err != nil {
			return MessageOutgoing{}, fmt.Errorf("error in handleValidateTextArea: %w", err)
		}
		return messageOutgoing, nil
	case "addTabButton":
		messageOutgoing, err := addTab(incomingMessage)
		if err != nil {
			return MessageOutgoing{}, fmt.Errorf("error in addTab: %w", err)
		}
		return messageOutgoing, nil
	case "removeTabButton":
		messageOutgoing, err := removeTab(incomingMessage)
		if err != nil {
			return MessageOutgoing{}, fmt.Errorf("error in removeTab: %w", err)
		}
		return messageOutgoing, nil
	default:
		return MessageOutgoing{}, errors.New("unhandled incomingMessage.ID: " + incomingMessage.ID)
	}
}

func handleValidateTextArea(incomingMessage MessageIncoming) (MessageOutgoing, error) {
	addAjaxDOM := make([]ModDOM, 1)
	putHTML := ""
	err := ValidateString(incomingMessage.Data)
	if err != nil {
		putHTML = `<p id="textareaValidation" style="background-color:red;color:black;padding:5px;margin:0px;">` + err.Error() + `</p>`
	}
	addAjaxDOM[0] = ModDOM{
		PageContext:    incomingMessage.PageContext,
		InsertSelector: "#" + incomingMessage.ID,
		DeleteSelector: "#textareaValidation",
		Action:         "afterend",
		HTML:           putHTML,
	}
	messageOutgoing := MessageOutgoing{
		MessageID:  incomingMessage.MessageID,
		AddAjaxDOM: addAjaxDOM,
	}
	return messageOutgoing, nil
}

func addTab(incomingMessage MessageIncoming) (MessageOutgoing, error) {
	addAjaxDOM := make([]ModDOM, 1)
	// I would like to re-parse the template but I'm uncertain if WASM has text/template support
	// if it does, passing templates to the client is a TODO.
	// Specifically, use public/views/sidebar.html template by parsing it.
	// Make sure the id is unique and the content should start working properly.
	putHTML := `<div class="tab" id="addedTab">
    <input type="radio" id="tab-00" name="tab-group-1">
    <label for="tab-00">Added Tab</label>

    <div class="content">
        <p>non-unique IDs cause trouble if you add more than one tab dynamically</p>
    </div>
</div>`
	addAjaxDOM[0] = ModDOM{
		PageContext:    incomingMessage.PageContext,
		DeleteSelector: "",
		InsertSelector: ".tabs",
		Action:         "beforeend",
		HTML:           putHTML,
	}
	messageOutgoing := MessageOutgoing{
		MessageID:  incomingMessage.MessageID,
		AddAjaxDOM: addAjaxDOM,
	}
	return messageOutgoing, nil
}

func removeTab(incomingMessage MessageIncoming) (MessageOutgoing, error) {
	addAjaxDOM := make([]ModDOM, 1)
	putHTML := ""
	addAjaxDOM[0] = ModDOM{
		PageContext:    incomingMessage.PageContext,
		DeleteSelector: ".tab:last-child",
		InsertSelector: "",
		Action:         "afterend",
		HTML:           putHTML,
	}
	messageOutgoing := MessageOutgoing{
		MessageID:  incomingMessage.MessageID,
		AddAjaxDOM: addAjaxDOM,
	}
	return messageOutgoing, nil
}
