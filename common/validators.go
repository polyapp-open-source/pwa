package common

import "errors"

func ValidateString(s string) error {
	if len(s) > 5 {
		return errors.New("String too long")
	}
	return nil
}
